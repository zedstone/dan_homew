let btn = document.querySelectorAll('.btn');

document.addEventListener('keydown', function(event) {


    for(let i =0 ; i < btn.length; i++){

        if(btn[i].classList.contains('highlighted')){
            btn[i].classList.remove('highlighted');
        }

        if (btn[i].textContent.toLocaleUpperCase() === event.key.toLocaleUpperCase()){
            btn[i].classList.add('highlighted');

        }
    }

});