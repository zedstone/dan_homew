

let user = createNewUser();

function createNewUser() {
    return {
        firstname: prompt('enter your name'),
        lastname: prompt('enter your lastname'),
        birthday: prompt('date of your birth? (in form: dd.mm.yyyy)'),

        getLogin(){

            return `${this.firstname[0].toLowerCase()}.${this.lastname.toLowerCase()}`
        },
        getPassword(){
            return `${this.firstname[0].toUpperCase()}${this.lastname.toLowerCase()}${this.birthday.substr(-4,4)}`
        },
        getAge(){

            let date = new Date();
            let userDate = new Date();
            userDate.setFullYear(Number(this.birthday.substr(-4,4)));
            userDate.setMonth(Number((this.birthday.substr(3,2)))-1);
            userDate.setDate(Number(this.birthday.substr(0,2)));
            return (Math.floor((date-userDate)/31536000000));
        }

    }
}

console.log(user);
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user.getAge());

