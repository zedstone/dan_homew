let number1;
let number2;
let operation;

do {
    number1 = prompt('enter your number 1');
} while (isNaN(+number1) || number1 === '' || !number1); /* Тут продолжить*/

do {
    number2 = prompt('enter your number 2');
} while (isNaN(+number2) || number2 === '' || !number2);


do {
    operation = prompt('enter your operation: + , - , * , /');
} while (!(operation === '/' || operation === '-' || operation === '*' || operation === '+'));


function calculeteTwoNumbers(num1, num2, operation) {
    let result;
    if (operation === '+') {
        result = +num1 + +num2;
    }
    if (operation === '-') {
        result = +num1 - +num2;
    }
    if (operation === '*') {
        result = +num1 * +num2;
    }
    if (operation === '/') {
        result = +num1 / +num2;
    }

    return result;
}

console.log(calculeteTwoNumbers(number1, number2, operation));