let testArr = ['hello', 'world', 23, '23', null, true];
filterBy(testArr, 'number');
filterBy(testArr, 'string');
filterBy(testArr, 'boolean');

function filterBy(arr, type) {
    let newArr = arr.filter(function(item) { return typeof item !== type; });
    console.log(newArr);
}