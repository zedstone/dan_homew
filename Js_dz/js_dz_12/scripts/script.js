let wrapper = document.querySelector('.images-wrapper');
let imgs = document.querySelectorAll('.image-to-show');
let currentSlide = 0;
let timer = setInterval(nextImg,1000);
let isPlay = true;

let startBTN = document.createElement('button');
startBTN.type = 'button';
startBTN.innerText = 'start';
wrapper.after(startBTN);

let stopBTN = document.createElement('button');
stopBTN.type = 'button';
stopBTN.innerText = 'stop';
wrapper.after(stopBTN);

function nextImg() {
    imgs[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide+1)%imgs.length;
    imgs[currentSlide].className = 'image-to-show show';
}

function stop() {
    isPlay = false;
    clearInterval(timer);
}
function play() {
    isPlay = true;
    timer = setInterval(nextImg,1000);
}

stopBTN.onclick = function() {
    if(isPlay) {
        stop();
    }
};
startBTN.onclick = function() {
    if(!isPlay) {
        play();
    }
};