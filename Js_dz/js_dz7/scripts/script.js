let var1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let var2 = ['1', '2', '3', 'sea', 'user', 23];



function createList(list){
    let container = document.getElementById('app');
    let listU = document.createElement('ul');

    container.append(listU);

    let mapArr = list.map(function(item) {
        let listItem = document.createElement('li');
        listItem.innerText = item;
        listU.append(listItem);
    });

    return container;
}

createList(var1);
createList(var2);
