let div = document.querySelector('div');
let input = document.querySelector('input');
let span = document.createElement('span');
let DelButton = document.createElement('button');
let errorMessage = document.createElement('span');
div.appendChild(errorMessage);


input.addEventListener("blur", createSpan);
input.addEventListener("focus", deletSpan);

function createSpan(){
   let clientPrice = input.value.trim();
    if (isNaN(+clientPrice) || input.value <= 0 )
        return Error ();
    if (input.value.trim() === '' ) {
        return this;
    }
    else
        div.prepend(span);
        DelButton.addEventListener('click',(deletSpan));
        let currentPrice = document.querySelector('span');
        currentPrice.innerText = input.value;
        span.style = "display : block";

    span.appendChild(DelButton);
    DelButton.innerText='x';
    input.style = 'border-color: green';
    errorMessage.remove();
}
function deletSpan() {
    span.style = 'display: none';
    input.style = 'border-color: gray';
    input.value = "";
    DelButton.removeEventListener('click',(deletSpan));

}

function Error () {
    input.style = 'border-color: red';
    errorMessage.innerText= 'Please enter correct price';
    div.appendChild(errorMessage);
}