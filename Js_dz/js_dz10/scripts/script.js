const btn = document.querySelector('.button');
btn.type = 'button';
let userPassword = document.querySelector('.user-password');
let userPasswordConfirm = document.querySelector('.confirm-user-password');
const icon = document.querySelectorAll('i');
icon.forEach(i => i.addEventListener('click', changeIcon));
btn.addEventListener('click', compareInputValues);

console.log(userPassword);
function changeIcon() {
    this.classList.toggle('fa-eye-slash');
    console.log(this.previousElementSibling);

    if (this.classList.contains('fa-eye-slash')) {
        this.previousElementSibling.type = 'text'
    } else {
        this.previousElementSibling.type = 'password'
    }
}

function compareInputValues() {


    if(userPassword.value === userPasswordConfirm.value && userPassword.value !== "") {
        if(document.querySelector('span')){
            document.querySelector('span').remove();
        }
        welcomeInfo();
    } else {
        if(!(document.querySelector('span'))) {
            btn.previousElementSibling.insertAdjacentHTML("afterend", `<span>Wrong password, try again</span>`);
            document.querySelector('span').style.color = 'red';
            document.querySelector('span').style.marginBottom = '10px';
        }
    }
}
function welcomeInfo() {
    alert('You are welcome')
}
