$(document).ready(function(){

    $(".hide").click(function(){
        $(".popular-section .container").slideToggle();
    });
//
    let btn = $('.button-up');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 600) {
            btn.fadeIn(100);
        } else {
            btn.fadeOut(100);
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '600');
    });
//

    $('.menu li a').click(function(e) {

        var targetHref = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(targetHref).offset().top
        }, 1000);

        e.preventDefault();
    });

});