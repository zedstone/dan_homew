let idValue = -1;
let selectedMessages = [];
let tabsTitleLi = document.querySelectorAll('.tabs-title');
let contentli = document.querySelectorAll('.tabs-content li');
tabsTitleLi.forEach((tab, i) => {
    tab.setAttribute('data-id', i + 1);
    contentli[i].setAttribute('data-id', i + 1);
    contentli[i].remove();
    tab.addEventListener('click', menuSelect);
});

function menuSelect() {

    if (idValue > 0) selectedMessages[0].remove();
    tabsTitleLi.forEach(t => {
        t.classList.remove('active');
    });
    this.classList.toggle('active');
    idValue = +this.dataset.id;

    selectedMessages = [...contentli].filter(li => +(li.dataset.id) === idValue);
    document.querySelector('.tabs-content').append(selectedMessages[0]);
}
