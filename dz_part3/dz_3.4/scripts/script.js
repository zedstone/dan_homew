const cards = [];

class mainContainer {
    cards =[];
    constructor(){
        this._el = document.createElement('div');
        this._el.classList.add('container');
        let dragble = document.querySelector('.on-drag');

        console.log(dragble);
        this._el.addEventListener("dragover", e => {
            let dragble = document.querySelector('.on-drag');
            e.preventDefault();
            console.log(dragble);
            const afterElement = this.dragAfterElement(this._el, e.clientY);
            if(afterElement == null){
                this._el.appendChild(dragble);
            }else {
                this._el.insertBefore(dragble, afterElement);
            }


        });

        this._createBtn = document.createElement('button');
        this._createBtn.innerText = "Добавить карточку";
        this._createBtn.classList.add('createBtn');
        this._createBtn.addEventListener("click", () =>{
            const item = new ListItem('');
            item.appendTo(this._el);
            this.cards.push(item);
        });
        this._el.append(this._createBtn);

        this._sortBtn = document.createElement('button');
        this._sortBtn.innerText = "Cортировка";
        this._sortBtn.classList.add('sortBtn');
        this._el.append(this._sortBtn);
        this._sortBtn.addEventListener('click', this.sort.bind(this));
    }
    sort(){
        this.cards.forEach(card => card.remove());
        this.cards.sort((card1, card2) => {

            if (card1.getValue() < card2.getValue()){
                return -1;
            }
            if (card1.getValue() > card2.getValue()){
                return 1;
            }
            return 0;
        });

        this.cards.forEach(card => card.appendTo(this._el));
    }
    add(title){
        const item = new ListItem(title);
        item.appendTo(this._el);
        this.cards.push(item);
    }
    appendTo(container) {
        container.append(this._el);
    }
    dragAfterElement(container, y){
        const draggbleElements = [...container.querySelectorAll(".draggable:not(.on-drag)")]

        return draggbleElements.reduce((closest, child) =>{
        const box = child.getBoundingClientRect();
        const offset = y - box.top - box.height / 2;

            if(offset < 0 && offset > closest.offset){
                return {offset: offset, element: child}
            }
            else {
                return closest;
            }
        }, { offset: Number.NEGATIVE_INFINITY}).element
    }
}
class ListItem {
    constructor(title){
        this._el = document.createElement('div');
        this._el.classList.add("draggable");
        this._el.setAttribute("draggable", "true");
        this._el.addEventListener("dragstart", () => {
            this._el.classList.add("on-drag");
        });

        this._el.addEventListener("dragend", () => {
            this._el.classList.remove("on-drag");
        });
        this._input = document.createElement('input');
        this._el.append(this._input);
        this._input.value = title;
    }
    appendTo(container) {
        container.append(this._el);
    }
    remove(){
        this._el.remove();
    }
    getValue(){
        return this._input.value;
    }
    
}

const MainContainer = new mainContainer();
MainContainer.appendTo(document.body);
