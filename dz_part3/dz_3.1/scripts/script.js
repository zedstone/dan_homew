// availableSizes,Stuffings и Toppings вынесены в отдельные обьекты  это улучшает
// удобство взаимодействия с программой, то бишь можно легко изменить цену того или иного продукта без нужды лезть в класс.
var sizeProperties = {
    SIZE_SMALL: {
        price: 50,
        cal: 20
    },
    SIZE_LARGE: {
        price: 100,
        cal: 40
    },
};

var stuffingProperties = {
    STUFFING_CHEESE: {
        price: 10,
        cal: 20
    },
    STUFFING_SALAD: {
        price: 20,
        cal: 5
    },
    STUFFING_POTATO: {
        price: 15,
        cal: 10
    },
};
var toppingProperties = {
    TOPPING_SPICE: {
        price: 15,
        cal: 0
    },
    TOPPING_MAYO: {
        price: 20,
        cal: 5
    },
};

class Ham {
    constructor(size, stuffing){

    }
    _price

}
function Hamburger(size, stuffing) {
    var price = 0;
    var cal = 0;
    var ham_size = "";

    var stuffings = [];
    var toppings = [];
    try {
        if (!size) {
            throw new HamburgerException("Size is not given.");
        } else if (!sizeProperties.hasOwnProperty(size)) {
            throw new HamburgerException('wrong');
        } else {
            ham_size = size;
            price = price + sizeProperties[size].price;
            cal = cal + sizeProperties[size].cal;
        }

    } catch (e) {
        console.error(`${e.message}`);
    }

    try {
        if (!stuffing) {
            throw new HamburgerException("Stuffing is not given.");
        } else if (!stuffingProperties.hasOwnProperty(stuffing)) {
            throw new HamburgerException('Error stuffing');
        } else {
            stuffings.push(stuffing);
            price = price + stuffingProperties[stuffing].price;
            cal = cal + stuffingProperties[stuffing].cal;
        }

    } catch (e) {
        console.error(`${e.message}`);
    }

    this.addStuffing = function (stuffing) {
        try {
            if (!stuffing) {
                throw new HamburgerException("Stuffing is not given.");
            } else if (!stuffingProperties.hasOwnProperty(stuffing)) {
                throw new HamburgerException('Error stuffing');
            } else {
                stuffings.push(stuffing);
                price = price + stuffingProperties[stuffing].price;
                cal = cal + stuffingProperties[stuffing].cal;
            }
        } catch (e) {
            console.error(`${e.message}`);
        }
    };
    this.removeStuffing = function (stuffing) {
        try {
            if(!stuffing) {
                throw new HamburgerException("Stuffing is not given.");
            } else if (!stuffingProperties.hasOwnProperty(stuffing)) {
                throw new HamburgerException('wrong stuffing');
            } else {
                var index = stuffings.indexOf(stuffing);
                if (index > -1) {
                    stuffings.splice(index, 1);
                }
                price = price - stuffingProperties[stuffing].price;
                cal = cal - stuffingProperties[stuffing].cal;
            }
        } catch (e) {
            console.error(`${e.message}`);
        }

    };

    this.addTopping = function (topping) {
        try {
            if(!topping) {
                throw new HamburgerException("Topping is not given.");
            }else if (!toppingProperties.hasOwnProperty(topping)) {
                throw new HamburgerException('wrong topping');
            } else {
                toppings.push(topping);
                price = price + toppingProperties[topping].price;
                cal = cal + toppingProperties[topping].cal;
            }
        }catch (e) {
            console.error(`${e.message}`);
        }

    };
    this.removeTopping = function (topping) {
        try{
            if(!topping) {
                throw new HamburgerException("Topping is not given.");
            }else if (!toppingProperties.hasOwnProperty(topping)) {
                throw new HamburgerException('wrong topping');
            } else {
                var index = toppings.indexOf(topping);
                if (index > -1) {
                    toppings.splice(index, 1);
                }
                price = price - toppingProperties[topping].price;
                cal = cal - toppingProperties[topping].cal;
            }
        }catch (e) {
            console.error(`${e.message}`);
        }

    };

    this.getPrice = function () {
        return price + " грн";
    };

    this.getCal = function () {
        return cal + " калории";
    };

    this.getSize = function () {
        return ham_size;
    };
    this.getStuffing = function () {
        return stuffings;
    };
    this.getToppings = function () {
        return toppings;
    };
}

function HamburgerException(message) {
    return this.message = message;
}

var ham = new Hamburger("SIZE_SMALL", "STUFFING_SALAD");
console.log(ham);
ham.getPrice();
ham.addStuffing("STUFFING_POTATO");

console.log(ham.getPrice());
console.log(ham.getCal());

ham.removeStuffing("STUFFING_SALAD");


ham.addTopping("TOPPING_SPICE");
ham.addTopping("TOPPING_MAYO");
ham.removeTopping("TOPPING_MAYO");
console.log(ham.getPrice());
console.log(ham.getCal());
console.log(ham.getSize());
console.log(ham.getStuffing());
console.log(ham.getToppings());