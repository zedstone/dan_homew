const container = document.querySelector('.container');

class Game {
    constructor(level) {
        this.person = {
            name: 'player',
            score: 0,
        };
        this.computer = {
            name: 'Computer',
            score: 0,
        };

        this.scoreToWin = 50;
        this.winner = '';
    }
    renderUI(){

            container.innerHTML = `
            <div class="game-level-settings">
              <button class="btn-level chosen" id="btn-light" type="button">easy</button>
              <button class="btn-level" id="btn-hard" type="button">middle</button>
              <button class="btn-level" id="btn-professional" type="button">hard</button>
            </div>
            <button class="switchable" type="button" id="start-game" >START NEW GAME</button>
            <div class="score-section-wrapper">
            <h4>SCORE</h4>
              <div class="score-block">
                <span class="score-title">Player</span>
                <span class="score-player"></span>
              </div>
              
              <div class="score-block">
                <span class="score-title">Computer</span>
                <span class="score-computer"></span>
              </div>
            </div>
          </div>`;



            const btnStartGame = document.getElementById('start-game');
            btnStartGame.onclick = () => {
                this.person.score = 0;
                this.computer.score = 0;
                this.winner = '';

                const cellArray = document.querySelectorAll('td');
                cellArray.forEach(el => el.classList.remove('red', 'blue', 'green'));
                this.play();
           };

            this.GameLevel();
    }


    GameLevel() {

        const LevelButtons = document.querySelectorAll('.btn-level');
        LevelButtons.forEach(el => {
            el.addEventListener('click', e => {
               LevelButtons.forEach(el => el.classList.remove('chosen'));
                e.target.classList.add('chosen');

            });
        });
    }
    getGameLevel() {
        const level = document.querySelector('.chosen');


        if(level.innerHTML === 'hard'){
            return 500;
        }
        if(level.innerHTML === 'middle'){
            return 1000;
        }
        if(level.innerHTML === 'easy'){
            return 1500;
        }
    }



    renderGameZone() {

        this.getGameLevel();
        const mainTable = document.createElement('table');
        container.append(mainTable);
        mainTable.classList.add('main-table');
        mainTable.innerHTML = '';
        for (let i = 0; i < 10; i++) {
            const fieldRow = document.createElement('tr');
            fieldRow.classList.add('field-row');
            mainTable.append(fieldRow);
            for (let j = 0; j < 10; j++) {
                const fieldCell = document.createElement('td');
                fieldCell.classList.add('field-cell');
                fieldRow.append(fieldCell);
            }
        }

    }


    play() {
        this.renderGameZone();
        this.nextTurn();
    }
    nextTurn() {
        const freeCellArray = document.querySelectorAll('td:not(.blue)');
        const cellIndex = Math.floor((freeCellArray.length - 1) * Math.random());
        const currentCell = freeCellArray[cellIndex];
        currentCell.classList.add('blue');
        currentCell.onclick = e => this.scoresPerson(currentCell, e);
        setTimeout(this.watchCell.bind(this, currentCell),  this.getGameLevel());
    }
    scoresPerson(currentCell, e) {
        e.target.classList.add('green');
        currentCell.onclick = null;
        this.person.score = ++this.person.score;
        const playerScoreField = document.querySelector(".score-player");
        playerScoreField.innerText = this.person.score;
        this.checkWinner(this.person.score, this.computer.score);
    }
    watchCell(currentCell) {
        if (currentCell.classList.contains('green')) {
            !this.winner ? this.nextTurn() : '';
            return;
        }
        currentCell.onclick = null;
        currentCell.classList.add('red');
        this.computer.score = ++this.computer.score;
        const compScoreField = document.querySelector(".score-computer");
        compScoreField.innerText = this.computer.score;
        this.checkWinner(this.person.score, this.computer.score);
        !this.winner ? this.nextTurn() : '';
    }
    checkWinner(playerScore, computerScore) {

        if (playerScore === this.scoreToWin) {
            this.winner = "player";
            setTimeout(() => alert(`Congratulations! ${this.winner} wins!`));

            const btnAndInputArray = document.querySelectorAll('.switchable');
            return;
        }
        if (computerScore === this.scoreToWin) {
            this.winner = "computer";
            setTimeout(() => alert(` ${this.winner} wins!`));

            const btnAndInputArray = document.querySelectorAll('.switchable');
            return;
        }

    }
}

const myGame = new Game();
myGame.renderUI();