const container = document.querySelector('.app')
const a = document.createElement("div");
const b = document.createElement("div");
container.append(a);
container.append(b);

const req1 = $.getJSON("https://jsonplaceholder.typicode.com/users").done(response => {

    a.innerHTML = "https://jsonplaceholder.typicode.com/users"
});

const req2 = $.getJSON("https://jsonplaceholder.typicode.com/posts").done(response => {

    b.innerHTML = "https://jsonplaceholder.typicode.com/users"
});

$.when(req1, req2).done(([resp1], [resp2]) =>{
    console.log(resp1);
    console.log(resp2);
});