/////////////////// tabs
let buttons = document.querySelector('.works-tabs').children;
let itemes = document.querySelector('.works-images').children;

for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', function sortWorks() {

        for (let j = 0; j < buttons.length; j++) {
            buttons[j].classList.remove('works-active');
        }
        this.classList.add('works-active');
        const target = this.getAttribute('data-target');

        for (let l = 0; l < itemes.length; l++) {
            itemes[l].style.display = "none";
            if (itemes[l].getAttribute('data-id') === target) {
                if(!itemes[l].classList.contains('card-load-more')){
                    itemes[l].style.display = "block";
                }
            }
            if (target === 'all') {
                if(!itemes[l].classList.contains('card-load-more')){
                    itemes[l].style.display = "block";
                }
            }
        }
    });
}
///////////////////////////// Load more btn
let isLoadMorePressed = false;
let loadMoreBtn = document.querySelector('.load-more-btn');
let loadAnim = document.querySelector('#floatingBarsG');

loadMoreBtn.addEventListener('click', function () {
    loadMoreBtn.remove();
    loadAnim.style.display = 'block';
    console.log('anim');

    setTimeout(downLoadImg,2000);
    function downLoadImg(){
        let hideElements = document.querySelectorAll('.card-load-more');
        for (let i = 0; i < hideElements.length; i++) {
            hideElements[i].classList.remove('card-load-more');
            buttons[0].click();
            loadAnim.style.display = 'none';
        }
    }

});

/// Slider


let dots = document.querySelectorAll('.client-list-item'),
    dotsArea = document.querySelector('.testimonial-client-list'),
    slides = document.querySelectorAll('.active-client-card'),
    prev = document.querySelector('.slider-left'),
    next = document.querySelector('.slider-right'),
    slideIndex = 1;

showSlides(slideIndex);

function showSlides (n) {
    if (n < 1) {
        slideIndex = slides.length;
    } else if (n > slides.length) {
        slideIndex = 1;
    }
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = 'none';
    }
    for (let j = 0; j < dots.length; j++) {
        dots[j].classList.remove('dot-active');
    }
    slides[slideIndex - 1].style.display = 'flex';
    dots[slideIndex - 1].classList.add('dot-active');
}

function plusSlides (n) {
    showSlides(slideIndex += n);
}
function currentSlide (n) {
    showSlides(slideIndex = n)
}
prev.addEventListener('click', function () {
    plusSlides(-1);
});
next.addEventListener('click', function () {
    plusSlides(1);
});

dotsArea.onclick = function (e) {
    for (let i = 0; i < dots.length + 1; i++) {
        if (e.target.classList.contains('client-list-item-img') && e.target.parentElement === dots[i - 1]) {
            currentSlide(i);
        }
    }
};
//

let tabs = document.querySelector('.services-tabs-block').children;
let tabsInfo = document.querySelector('.services-description').children;


for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', function () {
        for (let j = 0; j < tabs.length; j++) {
            tabs[j].classList.remove('services-tab-active');
            tabsInfo[j].classList.remove('active');
        }
        this.classList.add('services-tab-active');
        const index = this.getAttribute('data-target');


        if (tabsInfo[i].getAttribute('data-index') === index) {
            tabsInfo[i].classList.add('active');
        }

    });
}