import React, {PureComponent} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import axios from 'axios';
import ProdactList from "./components/ProdactList/ProdactList";

class App extends PureComponent {

  state = {
    firstBtnText: "Buy",
    firstBtnBg: "redBtn",
    modalOpen: false,
    modalCloseButton: true,
    modalHeader: "Great choice",
    modalText: "Want to add a product to your cart?",
    modalActions:[
      {id:1, topic:"ОК"},
      {id:2, topic:"Cancel"},
    ],

    goodsList:[],

  }

  componentDidMount() {
          axios('http://localhost:3000/list.json').then(res => this.setState({goodsList: res.data}));

  }

  render() {
    const {firstBtnText, firstBtnBg, modalOpen, modalCloseButton, modalHeader, modalText,modalActions, goodsList} = this.state;
    return (
        <div className="App">

          <ProdactList goodsList={goodsList} btnText = {firstBtnText} btnBg = {firstBtnBg} BtnFunc = {this.firstBtnFunc}/>

          {modalOpen && <Modal modalOpen ={modalOpen} modalCloseButton={modalCloseButton} modalHeader ={ modalHeader} modalText={modalText} modalActions={modalActions}  closeModal ={this.closeModal}/>
          }
        </div>
    );
  }

  firstBtnFunc = () =>{
    this.setState({modalOpen:true});
  };
  closeModal = () =>{
    this.setState({modalOpen:false, secondmodalOpen: false});
  };
}

export default App;
