import React, {PureComponent} from 'react';
import "./Button.scss";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";

class Button extends PureComponent {
    render() {
        const {btnText, btnBg, BtnFunc} = this.props;

        return (
            <div>
                <button className={btnBg} onClick={BtnFunc}>{btnText}</button>
            </div>
        )
    }

}
Button.propTypes = {
    btnText: PropTypes.string.isRequired,
    btnBg:PropTypes.string.isRequired,
    BtnFunc:PropTypes.func.isRequired,
}

export default Button;