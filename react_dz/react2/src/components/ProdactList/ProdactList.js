import React, {Component} from 'react';
import ProdactItem from "../ProdactItem/ProdactItem";
import "./ProdactList.scss";

class ProdactList extends Component {
    render() {
        const {goodsList, btnText, btnBg, BtnFunc} = this.props;

        const products = goodsList.map(prodact =><ProdactItem key ={prodact.id} prodact={prodact} btnText = {btnText} btnBg = {btnBg} BtnFunc = {BtnFunc}/>);


        return (
            <div className="ProdactList">
                {products}
            </div>
        );
    }
}

export default ProdactList;