import React, {Component} from 'react';
import "./ProdactItem.scss";
import Button from "../Button/Button";

class ProdactItem extends Component {
    state = {
        isFavorites:false,
    }

    render() {
        const {prodact, btnText, btnBg, BtnFunc} = this.props;
        const {isFavorites} = this.state;

        return (
            <div className="ProdactItem">
                <img src={prodact.path} alt="product"/>
                <div>{prodact.name} {isFavorites ? <span onClick={this.addFavorites} className="star-chosen"></span> : <span onClick={this.addFavorites} className="star"></span>} </div>
                <div>{prodact.price} $</div>
                <Button btnText={btnText} btnBg={btnBg} BtnFunc={BtnFunc} />
            </div>
        );
    }

    addFavorites = () => {
        const {isFavorites} = this.state;
        const {product} = this.props;
        const serialProduct = JSON.stringify(product);
        console.log(serialProduct);

        if(isFavorites){
            this.setState({isFavorites:false});
        }else {
            this.setState({isFavorites:true});
            localStorage.setItem({serialProduct}, 'favorite')
        }

    }
}

export default ProdactItem;