import React, {PureComponent} from 'react';
import "./Modal.scss";
import PropTypes from "prop-types";

class Modal extends PureComponent {

    render() {
        const {modalHeader, modalText, modalCloseButton, modalActions, closeModal, modalType} = this.props;

        const modalButtons = modalActions
            .map(button => <button className="modalBtnAction" key ={button.id}>{button.topic}</button>)

        return (
            <div className={modalType} onClick={closeModal}>

                <div className="modal">
                    {!modalCloseButton ? <div className="modalHeader">{modalHeader}</div> : <div className="modalHeader">{modalHeader} <span onClick={closeModal} className="cross">X</span> </div>}
                <div className="modalText">{modalText}</div>
                    <div>
                        {modalButtons}
                    </div>

                </div>
            </div>
        )
    }

}

Modal.propTypes = {
    modalHeader:PropTypes.string.isRequired,
    modalText:PropTypes.string.isRequired,
    modalCloseButton:PropTypes.bool.isRequired,
    modalActions:PropTypes.array.isRequired,
    closeModal:PropTypes.func.isRequired,
    modalType:PropTypes.string,
}

export default Modal;