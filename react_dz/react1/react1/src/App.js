import React, {PureComponent} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends PureComponent {

  state = {
    firstBtnText: "Open first modal",
    firstBtnBg: "redBtn",

    secondBtnText: "Open second modal",
    secondBtnBg: "blueBtn",

    modalType:"type1 modalWrapper",

    modalOpen: false,
    modalCloseButton: true,
    modalHeader: "Do you want to delete this file?",
    modalText: "Once you delete this file, it won’t be possible to undo this action. \n" +
        "Are you sure you want to delete it?",
    modalActions:[
      {id:1, topic:"ОК"},
      {id:2, topic:"Cancel"},
    ],

    secondmodalOpen: false,
    secondmodalCloseButton: false,
    secondmodalHeader: "Second modal!",
    secondmodalText: "Best modal ever!",
    secondmodalActions:[
      {id:3, topic:"ОК"},
      {id:4, topic:"Cancel"},
      {id:5, topic:"More"},
    ],
  }

  render() {
    const {firstBtnText, firstBtnBg, secondBtnText, secondBtnBg, modalOpen, modalCloseButton, modalHeader, modalText,modalActions,
      secondmodalOpen, secondmodalCloseButton,secondmodalHeader,secondmodalText,secondmodalActions, modalType} = this.state;

    return (
        <div className="App">
          <Button btnText = {firstBtnText} btnBg = {firstBtnBg} BtnFunc = {this.firstBtnFunc}/>
          <Button btnText = {secondBtnText} btnBg = {secondBtnBg} BtnFunc = {this.secondBtnFunc}/>

          {modalOpen && <Modal modalOpen ={modalOpen} modalCloseButton={modalCloseButton} modalHeader ={ modalHeader} modalText={modalText} modalActions={modalActions} modalType={modalType} closeModal ={this.closeModal}/>
          }
          {secondmodalOpen && <Modal modalOpen ={secondmodalOpen} modalCloseButton={secondmodalCloseButton} modalHeader ={ secondmodalHeader} modalText={secondmodalText} modalActions={secondmodalActions} modalType={modalType} closeModal ={this.closeModal}/>
          }
        </div>
    );
  }

  firstBtnFunc = () =>{
    this.setState({modalType: "type1 modalWrapper"});
    this.setState({modalOpen:true});
  };
  secondBtnFunc = () =>{
    this.setState({modalType: "type2 modalWrapper"});
    this.setState({secondmodalOpen:true});
  };
  closeModal = () =>{
    this.setState({modalOpen:false, secondmodalOpen: false});
  };
}

export default App;
